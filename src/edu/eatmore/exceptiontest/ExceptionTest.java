package edu.eatmore.exceptiontest;

public class ExceptionTest {

    public static void main(String[] args) {
        int n = 20000;
        long min = Long.MAX_VALUE;
        long max = Long.MIN_VALUE;
        long sum = 0;
        for (int i = 0; i < n; i++) {
            long time = System.nanoTime();
            try {
                f(0);
            } catch (RuntimeException e) {
                //e.getStackTrace();
            }
            time = System.nanoTime() - time;
            //System.out.println(time);
            sum += time;
            if (time < min) {
                min = time;
            }
            if (time > max) {
                max = time;
            }
        }
        System.out.println("MIN: " + min);
        System.out.println("MAX: " + max);
        System.out.println("AVG: " + (sum / n));
    }

    public static void f(int counter) {
        if (counter > 3000) {
            lol();
        } else {
            g(counter + 1);
        }
    }

    public static void g(int counter) {
        if (counter > 3000) {
            lol();
        } else {
            h(counter + 1);
        }
    }

    public static void h(int counter) {
        if (counter > 3000) {
            lol();
        } else {
            f(counter + 1);
        }
    }

    public static void lol() {
        //Date date = null;
        //date.getTime();
        throw new MyException();
    }
}
